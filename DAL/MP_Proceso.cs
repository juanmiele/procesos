﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace DAL
{
    public class MP_Proceso
    {

        private Acceso acceso = new Acceso();
        DataSet ds = new DataSet();
        public void Insertar(BE.Proceso proceso)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nom", proceso.Nombre));
            parameters.Add(acceso.CrearParametro("@min", proceso.Tiempo));
            acceso.Escribir("PROCESO_INSERTAR", parameters);

            acceso.Cerrar();
        }


        public List<BE.Proceso> Listar()
        {
            List<BE.Proceso> lista = new List<BE.Proceso>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("PROCESOS_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Proceso p = new BE.Proceso();
                p.Nombre = registro[1].ToString();
                p.Id = int.Parse(registro[0].ToString());
                p.Tiempo = (registro[2].ToString());
                lista.Add(p);
            }
            return lista;
        }
        public void grabarXML(BE.Proceso proceso)
        {
            //ds.Clear();
            ds.Tables.Clear();
            if (File.Exists("C:\\xml\\procesos.xml"))
            {
                ds.ReadXml("C:\\xml\\procesos.xml");
                DataRow registro = ds.Tables[0].NewRow();
                registro[0] = proceso.Nombre;
                registro[1] = proceso.Tiempo;
                ds.Tables[0].Rows.Add(registro);
                ds.WriteXml("C:\\xml\\procesos.xml");
            }
            else
            {
                ds.Tables.Clear();
                ds.Tables.Add(new DataTable("Proceso"));
                ds.Tables["Proceso"].Columns.Add(new DataColumn("Nombre"));
                ds.Tables["Proceso"].Columns.Add(new DataColumn("Tiempo"));
                ds.WriteXmlSchema("C:\\xml\\Esquema");
                DataRow registro = ds.Tables[0].NewRow();
                registro[0] = proceso.Nombre;
                registro[1] = proceso.Tiempo;
                ds.Tables[0].Rows.Add(registro);
                ds.WriteXml("C:\\xml\\procesos.xml");
            }
        }
        public DataSet ListarXML()
        {
            List<BE.Proceso> procesos = new List<BE.Proceso>();
            ds.Clear();
            if (File.Exists("C:\\xml\\procesos.xml"))
            {
                ds.ReadXml("C:\\xml\\procesos.xml");
            }
            return ds;

        }
    }
}

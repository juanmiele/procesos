﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Data;
using BE;




namespace BLL
{
    public class Proceso
    {
        DAL.MP_Proceso mp = new DAL.MP_Proceso();
        

        public bool EjecutarProceso(string proceso, string parametro = null)
        {
            bool ok = true;
            try
            {
                Process.Start(proceso, parametro);
            }
            catch
            {
                ok = false;
            }
            return ok;
        }
        public void MatarProceso(Process proceso)
        {
            proceso.Kill();
        }
        public Process[] LeerProcesos()
        {
            Process[] procesos = Process.GetProcesses();

            return procesos;
        }
            public void grabarProceso(Process proceso)
        {
            BE.Proceso grabar = new BE.Proceso();
            grabar.Id = proceso.Id;
            grabar.Nombre = proceso.ProcessName;
                DateTime inicio;
                DateTime actual;
                TimeSpan duracion;
            try
            { 
                inicio = proceso.StartTime;
                actual = System.DateTime.Now;
                duracion = actual - inicio;
                grabar.Tiempo = Convert.ToDateTime(duracion.ToString(@"hh\:mm")).ToShortTimeString();     
            }
            catch
            {
                grabar.Tiempo = "00:00";
            }
            GrabarDB(grabar);
            GrabarXML(grabar);

        }

        
        public void GrabarDB(BE.Proceso proceso)
        {
            mp.Insertar(proceso);
        }
        public void GrabarXML(BE.Proceso proceso)
        {
            mp.grabarXML(proceso);
        }


        public List<BE.Proceso> Listar()
        {
            return mp.Listar();
        }

        public DataSet Listarxml()
        {
            return mp.ListarXML();
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace Presentacion
{
    public partial class Form1 : Form
    {
        BLL.Proceso GestorProcesos = new BLL.Proceso();
        DataSet ds = new DataSet();

        public Form1()
        {
            InitializeComponent();
        }

        
        private void TraerProcesos()
        {
            listBox1.DataSource = null;
            listBox1.DataSource = GestorProcesos.LeerProcesos();
            listBox1.DisplayMember = "processName";
        }


        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TraerProcesos();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                Process p = (Process)listBox1.SelectedItem;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        { if (textBox1.Text == "")
            {
                MessageBox.Show("Ingrese el nombre del proceso");
            }
            else
            {
                if (GestorProcesos.EjecutarProceso(textBox1.Text, textBox2.Text)==false)
                {
                    MessageBox.Show("El proceso no existe");
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                Process p = (Process)listBox1.SelectedItem;
                GestorProcesos.MatarProceso(p);
                TraerProcesos();
            }
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                Process p = (Process)listBox1.SelectedItem;
                    GestorProcesos.grabarProceso(p);
                 Enlazar();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            BuscarPrograma(textBox3.Text);
        }
        private void BuscarPrograma(string proceso)
        {
            if (!string.IsNullOrEmpty(proceso))
            {
                int index = listBox1.FindString(proceso);
                if (index != -1)
                    listBox1.SetSelected(index, true);
                else
                    MessageBox.Show("No se escuentra el proceso");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Enlazar();
        }
        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = GestorProcesos.Listar();
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = GestorProcesos.Listarxml().Tables[0];
        }
    }
}

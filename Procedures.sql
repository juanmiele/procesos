USE [procesos]
GO

/****** Object:  StoredProcedure [dbo].[PROCESO_INSERTAR]    Script Date: 25/11/2020 07:34:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[PROCESO_INSERTAR]
           @nom nchar(50),
           @min time
as
BEGIN 
declare @id int

set @id  = isnull( (SELECT MAX(Id_Proceso) from Procesos) ,0) +1

insert Procesos values (@id, @Nom, @min)


END
GO


